import React from "react";
import {shallow} from "enzyme";
import App from "../App";
import ParcelForm from "../ParcelForm";

describe("App", () => {
  it("renders a form to input the parcel's tracking code", () => {
    const wrapper = shallow(<App />);
    expect(wrapper.containsMatchingElement(<ParcelForm />)).toEqual(true);
  });
});