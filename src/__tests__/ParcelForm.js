import React from "react";
import {shallow} from "enzyme";
import ParcelForm from "../ParcelForm";
import {Button, Form} from "react-bootstrap";

describe("ParcelForm", () => {
  it("contains the text input for parcel number", () => {
    const wrapper = shallow(<ParcelForm />);
    expect(wrapper.containsMatchingElement(<Form.Control />)).toEqual(true);
  });

  it("contains the submit button", () => {
    const wrapper = shallow(<ParcelForm />);
    expect(wrapper.containsMatchingElement(<Button type="submit">Prześlij</Button>)).toEqual(true);
  });
});
