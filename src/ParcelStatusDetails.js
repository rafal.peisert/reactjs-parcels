import React from "react";
import {Table} from "react-bootstrap";
import Moment from "react-moment";
import ParcelStatusPopover from "./ParcelStatusPopover";

function ParcelStatusDetails(props) {
  return (
    <Table striped bordered hover size="sm">
      <tbody>
      <tr>
        <td className="text-right">Typ przesyłki:</td>
        <td className="text-left">{props.parcelType}</td>
      </tr>
      <tr>
        <td className="text-right">Status:</td>
        <td className="text-left">
          {props.parcelStatus.title}{" "}
          <ParcelStatusPopover
            id={`psd-${props.parcelCreated}`}
            title={props.parcelStatus.title}
            description={props.parcelStatus.description}
          />
        </td>
      </tr>
      <tr>
        <td className="text-right">Utworzono:</td>
        <td className="text-left">
          <Moment local withTitle format="dddd, DD.MM.YYYY HH:mm">
            {props.parcelCreated}
          </Moment>
        </td>
      </tr>
      <tr>
        <td className="text-right">Zaktualizowano:</td>
        <td className="text-left">
          <Moment local withTitle format="dddd, DD.MM.YYYY HH:mm">
            {props.parcelUpdated}
          </Moment>
        </td>
      </tr>
      </tbody>
    </Table>
  );
}

export default ParcelStatusDetails;
