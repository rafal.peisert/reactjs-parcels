import React from "react";
import {Col, Row} from "react-bootstrap";
import Moment from "react-moment";
import ParcelStatusPopover from "./ParcelStatusPopover";

function ParcelStatusChange(props) {
  return (
    <Row className="text-left align-items-center">
      <Col xs={6} className="text-right">
          <Moment local withTitle format="dddd, DD.MM.YYYY HH:mm">
            {props.status.datetime}
          </Moment>
      </Col>
      <Col xs={6}>
        {props.details.title}{" "}
        <ParcelStatusPopover
          id={`psc-${props.status.datetime}`}
          title={props.details.title}
          description={props.details.description}
        />
      </Col>
    </Row>
  );
}

export default ParcelStatusChange;
