import React from "react";
import {OverlayTrigger, Popover} from "react-bootstrap";
import {HelpCircle} from "react-feather";

function ParcelStatusPopover(props) {
  return (
    <OverlayTrigger
      trigger="click"
      placement="left"
      overlay={
        <Popover id={props.id}>
          <Popover.Title>{props.title}</Popover.Title>
          <Popover.Content>{props.description}</Popover.Content>
        </Popover>
      }
    >
      <HelpCircle size={16} className="text-primary" cursor="pointer"/>
    </OverlayTrigger>
  );
}

export default ParcelStatusPopover;
