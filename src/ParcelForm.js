import React, {useState} from "react";
import {Button, Col, Form, Row} from "react-bootstrap";
import ParcelStatus from "./ParcelStatus";

function ParcelForm() {
  const [parcelNumber, setParcelNumber] = useState("");
  const [displayInfo, setDisplayInfo] = useState(false);

  const handleSubmitClick = event => {
    event.preventDefault();
    if (! parcelNumber) {
      return;
    }
    setDisplayInfo(true);
  }

  const handleParcelNumberChange = event => {
    setDisplayInfo(false);
    setParcelNumber(event.target.value);
  }

  return (
    <>
      <h1 className="text-center h3 mt-5">Śledzenie przesyłki InPost</h1>
      <Form className="my-1" method="post">
        <Form.Row className="align-items-center">
          <Col xs={0} lg={3}>{}</Col>
          <Col xs={8} lg={6}>
            <Form.Label htmlFor="parcelNumber" srOnly>Numer przesyłki</Form.Label>
            <Form.Control
              id="parcelNumber"
              placeholder="Numer przesyłki"
              value={parcelNumber}
              onChange={handleParcelNumberChange}
              required
            />
          </Col>
          <Col xs="auto">
            <Button type="submit" onClick={handleSubmitClick}>Prześlij</Button>
          </Col>
        </Form.Row>
      </Form>
      <Row className="text-center mt-2">
        <Col xs={0} lg={2}>{}</Col>
        <Col xs={12} lg={8}>
          {displayInfo ? <ParcelStatus parcelNumber={parcelNumber} /> : ""}
        </Col>
      </Row>
    </>
  );
}

export default ParcelForm;
