import React from 'react';
import ParcelForm from "./ParcelForm";
import {Container} from "react-bootstrap";

function App() {
  return (
    <div className="App">
      <Container>
        <ParcelForm />
      </Container>
    </div>
  );
}

export default App;
