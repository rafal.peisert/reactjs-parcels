import React, {useEffect, useState} from "react";
import axios from "axios";
import {Alert, Spinner} from "react-bootstrap";
import ParcelStatusDetails from "./ParcelStatusDetails";
import ParcelStatusChange from "./ParcelStatusChange";

function ParcelStatus(props) {
  const [parcelData, setParcelData] = useState({})
  const [serviceTypes, setServiceTypes] = useState([])
  const [statusDetails, setStatusDetails] = useState({})
  const [loading, setLoading] = useState(true)
  const [loadingError, setLoadingError] = useState(false)

  useEffect(() => {
    Promise.all([
      axios.get(`https://api-shipx-pl.easypack24.net/v1/tracking/${props.parcelNumber}`),
      axios.get("https://api-shipx-pl.easypack24.net/v1/services"),
      axios.get("https://api-shipx-pl.easypack24.net/v1/statuses")
    ]).then(responseArray => {
      setParcelData(responseArray[0].data);
      setServiceTypes(responseArray[1].data);
      setStatusDetails(responseArray[2].data);
    }).catch((err) => {
      console.log(err);
      setLoadingError(true);
    }).then(() => setLoading(false));
  }, [props.parcelNumber]);

  function getServiceName(id) {
    const item = serviceTypes.find(item => item.id === id)
    return item ? item.name : "Nieznany typ przesyłki"
  }

  function getStatusDetails(name) {
    const item = statusDetails.items.find(item => item.name === name)
    return item || {title: "Nieznany status", name: name, description: "Nierozpoznany status"}
  }

  if (loading) {
    return (
      <div className="text-center mt-2">
        <Spinner animation="border" role="status">
          <span className="sr-only">Pobieranie danych przesyłki...</span>
        </Spinner>
      </div>
    )
  }

  if (loadingError) {
    return (
      <Alert variant="warning" dismissible className="mt-4">
        Nie znaleziono przesyłki lub wystąpił problem z połączeniem.
        Sprawdź numer i spróbuj ponownie.
      </Alert>
    )
  }

  let rows = []
  // noinspection JSUnresolvedVariable
  for (const status of parcelData.tracking_details) {
    const details = getStatusDetails(status.status);
    rows.push(
      <ParcelStatusChange status={status} details={details} key={status.datetime} />
    )
  }

  // noinspection JSUnresolvedVariable
  return (
    <>
      <h2 className="text-center h4 mt-4">Status przesyłki nr {props.parcelNumber}</h2>
      <ParcelStatusDetails
        parcelType={getServiceName(parcelData.type)}
        parcelStatus={getStatusDetails(parcelData.status)}
        parcelCreated={parcelData.created_at}
        parcelUpdated={parcelData.updated_at}
      />
      <h2 className="text-center h4 mt-4">Zmiany statusu</h2>
      {rows}
    </>
  );
}

export default ParcelStatus;
